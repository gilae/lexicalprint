/**
 [Nickolai Vassiliev]
 File: strecho.c
 Reads in sequences of characters, seperates them by 
  spaces and prints out the words in lexicological 
  order.
**/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

struct list {
 	char ** dat;
  int size;
  int len;
};

struct inpbuffer {
  char * dat;
  int size;
  int len;
};

/*
	This program runs in O(nlogn) 
*/

void copy_string(char ** a, int len, char ** b){
  for (int i = 0; i < len; i++){
    free(b[i]);
		b[i] = malloc(sizeof(char)*(strlen(a[i])+1));
    strcpy(b[i],a[i]);
  }
}
void msort_helper(char ** a, int len, int med){
  int i = 0;
  int o = 0;
  int p = med;
  char ** temparray = malloc(len*sizeof(char *));
  while (o < len){
    if (p == len){
      temparray[o] = malloc(sizeof(char)*
                            (strlen(a[i])+1));
      strcpy(temparray[o],a[i]);
      i++;
    }
    else if (i == med){
      temparray[o] = malloc(sizeof(char)*
                            (strlen(a[p])+1));
      strcpy(temparray[o],a[p]);
      p++;
    }
    else if (strcmp(a[p],a[i]) < 0){
      temparray[o] = malloc(sizeof(char)*
                            (strlen(a[p])+1));
      strcpy(temparray[o],a[p]);
      p++;
    }
    else {
      temparray[o] = malloc(sizeof(char)*
                            (strlen(a[i])+1));
      strcpy(temparray[o],a[i]);
      i++;
    }
    o++;
  }
  copy_string(temparray,len,a);
  for (int i = 0; i< len; i++){
    free(temparray[i]);
  }
  free(temparray);
}

void msort(char ** a,int len){
	if (len < 2){
    return;
  }
  int med = len/2;
  msort(a,med);
  msort(a+med,len-med);
  msort_helper(a, len, med);
}

int main() {
  struct list slist = {malloc(sizeof(char *)), 1, 0};
	struct inpbuffer buffer = {malloc(sizeof(char)),1,0};
  char inp = getchar();
  while (inp != EOF){
    if (isspace(inp)){
      if (buffer.len > 0){
        if (slist.len == slist.size){
          slist.size = slist.size*2;
          slist.dat = realloc(slist.dat,
                    sizeof(char *)*slist.size);
        }
        buffer.dat = realloc(buffer.dat,
                          sizeof(char)*buffer.len+1);
        buffer.dat[buffer.len]='\0';
        slist.dat[slist.len] = malloc(sizeof(char)
                                      *buffer.len+1);
        strcpy(slist.dat[slist.len],buffer.dat);
        (slist.len)++;
      }
      buffer.len = 0;
      buffer.size = 1;
      free(buffer.dat);
      buffer.dat = malloc(sizeof(char));
    }
    else {
    	if (buffer.len == buffer.size){
    		buffer.size = buffer.size*2;
        buffer.dat = realloc(buffer.dat,
                            sizeof(char)*buffer.size);
     	}
      buffer.dat[buffer.len] = inp;
      (buffer.len)++;
    }
   	inp=getchar();
  }
  if (buffer.len > 0){
    if (slist.len == slist.size){
      slist.size = slist.size*2;
      slist.dat = realloc(slist.dat,
                          sizeof(char *)*slist.size);
    }
    buffer.dat = realloc(buffer.dat,
                        sizeof(char)*buffer.len+1);
    buffer.dat[buffer.len]='\0';
    slist.dat[slist.len] =
      malloc(sizeof(char)*sizeof(char)*buffer.len+1);
    strcpy(slist.dat[slist.len],buffer.dat);
		(slist.len)++;
  }
  msort(slist.dat,slist.len);
 	for (int i = 0;i<slist.len;i++){
    printf("%s\n",slist.dat[i]);
  }
  for (int i = 0; i < slist.len; i++){
    free(slist.dat[i]);
  }
  free(slist.dat);
  free(buffer.dat);
}


